package UnitOfWork;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Id;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.cfg.Configuration;

import Database.Address;
import Database.Entity;
import Database.EntityState;


public class UnitOfWork {
 
		//session  zamiast connection
		private Session session;
		private Transaction transaction;

	  
		private Map<Entity, iUnitOfWorkRepository> entities = 
				new HashMap<Entity, iUnitOfWorkRepository>();
		
				
		public UnitOfWork(Session session) {
					super();
					this.setSession(session);
			        this.transaction = session.getTransaction();

				}
	   
				
		public void saveChanges() throws Exception {
			 transaction.begin();
			try{
				for(Entity entity: entities.keySet())
			{
				switch(entity.getState())
				{
				case Modified:
					entities.get(entity).persistUpdate(entity);
					break;
				case Deleted:
					entities.get(entity).persistDelete(entity);
					break;
				case New:
					entities.get(entity).persistAdd(entity);
					break;
				case UnChanged:
					break;
				default:
					break;}
			}
				 transaction.commit();
		            entities.clear();
			}catch(TransactionException e){
				e.printStackTrace();
				undo();
			}
		}
			
		

		public void undo() {
			 entities.clear();
		     transaction.rollback();
			
		}

		
		public void markAsNew(Entity entity, iUnitOfWorkRepository repo) {
			 entity.setState(EntityState.New);
		      entities.put(entity,repo);
		}
		
		public void markAsDelete(Entity entity, iUnitOfWorkRepository repo) {
			entity.setState(EntityState.Deleted);
		      entities.put(entity,repo);
		}

		
		public void markAsChanged(Entity entity, iUnitOfWorkRepository repo) {
			entity.setState(EntityState.Modified);
		      entities.put(entity,repo);
		}


		public Session getSession() {
			return session;
		}


		public void setSession(Session session) {
			this.session = session;
		}
	

		
		  

		
}
