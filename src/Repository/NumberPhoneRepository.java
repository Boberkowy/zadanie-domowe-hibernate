package Repository;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import Database.Entity;
import Database.NumberPhone;
import Database.Person;
import Repository.Interfaces.iNumberPhoneRepository;
import UnitOfWork.iUnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;

public class NumberPhoneRepository  extends Repository<NumberPhone> implements iNumberPhoneRepository, iUnitOfWorkRepository {

	

	private Session session;
    private iUnitOfWork unitOfWork;
    private Query query;
    

	public NumberPhoneRepository(iUnitOfWork unitOfWork) {
		super(unitOfWork);
		this.session = unitOfWork.getSession();
		this.unitOfWork = unitOfWork;
	
	}
    
    @Override
	public void allOnPage(PagingInfo page) {
        PagingInfo<NumberPhone> pagingInfo = new PagingInfo<>(getAll());
        System.out.println(pagingInfo);
		
	}

	@Override
	public void add(NumberPhone entity) {
			unitOfWork.markAsNew(entity,this);
	}

	@Override
	public void delete(NumberPhone entity) {
		unitOfWork.markAsDelete(entity,this);
		
	}

	@Override
	public void modify(NumberPhone entity) {
		unitOfWork.markAsChanged(entity,this);
		
	}

	@Override
	public List<NumberPhone> getAll() {
		String sql = "FROM NumberPhone";
		query = session.createQuery(sql);
		
		return query.list();
	}

	@Override
	public int count() {
		return getAll().size();
	}

	@Override
	public void withId(int Id) {
		String sql = "FROM NumberPhone WHERE Id = :Id";
		query = session.createQuery(sql);
		query.setParameter("Id", Id);
		System.out.println(query.list());
	}

	@Override
	public void withNumber(long number) {
		String sql = "FROM NumberPhone WHERE Number = :number";
		query = session.createQuery(sql);
		query.setParameter("Number", number);
		System.out.println(query.list());		
	}

	@Override
	public void withPerson(Person person) {
		String sql = "FROM NumberPhone WHERE Person = :person";
		query = session.createQuery(sql);
		query.setParameter("Person", person);
		System.out.println(query.list());	
		
	}

	@Override
	protected void setUpUpdateQuery(NumberPhone entity) throws SQLException {
		session.update(entity.getNumberPhoneId());
		session.update(entity.getCityPrefix());
		session.update(entity.getCountryPrefix());
		session.update(entity.getTypeId());
		session.update(entity.getNumber());

	}

	@Override
	protected void setUpInsertQuery(NumberPhone entity) throws SQLException {
		session.persist(entity.getNumberPhoneId());
		session.persist(entity.getCityPrefix());
		session.persist(entity.getCountryPrefix());
		session.persist(entity.getTypeId());
		session.persist(entity.getNumber());

		
	}

	@Override
	protected String getTableName() {
		return "NumberPhone";
	}

	@Override
	protected String getUpdateQuery() {
		 return "UPDATE NumberPhone SET"
			 		+ "(numberPhoneId,countryPrefix, cityPrefix, number, typeId)"
			 		+ "=(?,?,?,?,?)";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO NumberPhone VALUES"
		 		+ "(numberPhoneId,countryPrefix, cityPrefix, number, typeId)"
		 		+ "=(?,?,?,?,?)";
	}

	
	
}
