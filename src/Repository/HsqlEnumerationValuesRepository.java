package Repository;


import java.sql.SQLException;
import java.util.List;

import javax.swing.plaf.basic.BasicComboPopup.InvocationKeyHandler;

import org.hibernate.Query;
import org.hibernate.Session;

import Database.Address;
import Database.Entity;
import Database.EnumerationValue;

import Repository.Interfaces.iEnumerationValueRepository;
import UnitOfWork.UnitOfWork;
import UnitOfWork.iUnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;

public class HsqlEnumerationValuesRepository extends Repository<EnumerationValue> implements iEnumerationValueRepository, iUnitOfWorkRepository  {
	
	private Session session;
	private iUnitOfWork unitOfWork;
	private Query query;
	
	public  HsqlEnumerationValuesRepository(iUnitOfWork unitOfWork) {
		super(unitOfWork);
		this.session = unitOfWork.getSession();
		this.unitOfWork = unitOfWork;
	}


	@Override
	public void add(EnumerationValue entity) {
		unitOfWork.markAsNew(entity,this);
	}

	@Override
	public void delete(EnumerationValue entity) {
		unitOfWork.markAsDelete(entity, this);
		
	}

	@Override
	public void modify(EnumerationValue entity) {
		unitOfWork.markAsChanged(entity, this);
		
	}

	@Override
	public List<EnumerationValue> getAll() {
		String sql = "FROM EnumerationValue";
		query = session.createQuery(sql);
		return query.list();
	}

	@Override
	public int count() {
		return getAll().size();
	}


	@Override
	public void withName(String name) {
		String sql = "FROM EnumerationValue WHERE enumerationName = :name";
		query=session.createQuery(sql);
		query.setParameter("name", name);
		System.out.println(query.list());
	}

	@Override
	public void withintKey(int key, String name) {
		String sql = "FROM EnumerationValue WHERE enumerationName = :name AND intkey = :intkey";
		query=session.createQuery(sql);
		query.setParameter("name", name);
		query.setParameter("intkey", key);
		System.out.println(query.list());		
	}

	@Override
	public void withStringKey(String key, String name) {
		String sql = "FROM EnumerationValue WHERE enumerationName = :name AND stringKey = :stringkey";
		query = session.createQuery(sql);
		query.setParameter("name", name);
		query.setParameter("stringKey", key);
		System.out.println(query.list());
	}



	@Override
	public void withId(int id) {
		String sql = "FROM EnumerationValue WHERE IntKey = :id";
		query = session.createQuery(sql);
		query.setParameter("IntKey", id);
		System.out.println(query.list());
		
	}



	@Override
	public void allOnPage(PagingInfo page) {
		 PagingInfo<EnumerationValue> pagingInfo = new PagingInfo<>(getAll());		
		
	}


	@Override
	protected void setUpUpdateQuery(EnumerationValue entity) throws SQLException {
		session.update(entity.getEnumerationName());
		session.update(entity.getStringKey());
		session.update(entity.getValue());
		session.update(entity.getIntkey());
		
	}


	@Override
	protected void setUpInsertQuery(EnumerationValue entity) throws SQLException {
		session.persist(entity.getEnumerationName());
		session.persist(entity.getStringKey());
		session.persist(entity.getValue());
		session.persist(entity.getIntkey());
		
	}


	@Override
	protected String getTableName() {
		return "EnumerationValue";
	}


	@Override
	protected String getUpdateQuery() {
		return "UPDATE EnumerationValue SET"
		 		+ "(addressId,countryId, regionId, city, street houseNumber, localNumber, zipCode, typeId)"
		 		+ "=(?,?,?,?,?,?,?,?,?)";
	}


	@Override
	protected String getInsertQuery() {
		return "INSERT INTO EnumerationValue "
		 		+ "(addressId,countryId, regionId, city, street houseNumber, localNumber, zipCode, typeId)"
		 		+ "=(?,?,?,?,?,?,?,?,?)";
	}




}

	
	