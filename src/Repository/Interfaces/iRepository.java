package Repository.Interfaces;

import java.util.List;

import Database.Entity;
import Repository.PagingInfo;



public interface iRepository <TEntity extends Entity>{

	public void withId(int id);
	public void allOnPage(PagingInfo page);
	public void add(TEntity entity);
	public void delete(TEntity entity);
	public void modify (TEntity entity);
	public List<TEntity> getAll();
    int count();
}
