package Repository.Interfaces;

import Database.Person;
import Database.User;

public interface iPersonRepository extends iRepository<Person>{

	void withId(int id);
    void withNameAndSurname(String name,String surname);
    void withPesel(long pesel);
    void withNip(long nip);
    void withEmail(String email);
    void withUser(User user);
}
