package Repository.Interfaces;

import java.util.List;

import Database.RolesPermissions;
import Database.User;
import Database.UserRoles;

public interface iUserRepository extends iRepository<User>{

	public void withLogin(String login);
	public void withLoginAndPassword(String login, String password);
	public void setupPermissions(User user);
	
}
