package Repository.Interfaces;

import Database.RolesPermissions;

public interface iRolesRepository extends iRepository<RolesPermissions> {
	
	void withId(int id);
    void withPermissionId(int id);
}
