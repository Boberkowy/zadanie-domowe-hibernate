package Repository.Interfaces;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.Entity;

public interface iEntityBuilder<TEntity extends Entity> {

	public TEntity build(ResultSet rs) throws SQLException;
}
