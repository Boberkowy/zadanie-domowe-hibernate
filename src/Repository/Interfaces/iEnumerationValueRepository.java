package Repository.Interfaces;


import Database.EnumerationValue;

public interface iEnumerationValueRepository extends iRepository<EnumerationValue> {

		public void withName(String name);
		public void withintKey(int key, String name);
		public void withStringKey(String key, String name);
}
