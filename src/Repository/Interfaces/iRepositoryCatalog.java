package Repository.Interfaces;

public interface iRepositoryCatalog {
	
	public iEnumerationValueRepository enumerations();
	public iUserRepository users();
	public iPersonRepository persons();
	public iAddressRepository addresses();
	public iNumberPhoneRepository numbers();
	public iRolesRepository roles();
	void commit();
}
