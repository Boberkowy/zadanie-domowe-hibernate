package Repository.Interfaces;

import Database.NumberPhone;
import Database.Person;

public interface iNumberPhoneRepository extends iRepository<NumberPhone> {

		void withId(int Id);
	    void withNumber(long number);
	    void withPerson(Person person);
}
