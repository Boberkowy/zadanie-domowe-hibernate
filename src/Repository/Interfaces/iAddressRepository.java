package Repository.Interfaces;

import Database.Address;
import Database.Person;

public interface iAddressRepository extends iRepository<Address>{
		
		
		void withId(int id);
	    void withCityAndStreet(String city, String street);
	    void withPerson(Person person);
}
