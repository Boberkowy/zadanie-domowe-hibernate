package Repository;

import Database.EnumerationValue;
import Repository.Interfaces.iAddressRepository;
import Repository.Interfaces.iEnumerationValueRepository;
import Repository.Interfaces.iNumberPhoneRepository;
import Repository.Interfaces.iPersonRepository;
import Repository.Interfaces.iRepositoryCatalog;
import Repository.Interfaces.iRolesRepository;
import Repository.Interfaces.iUserRepository;
import UnitOfWork.iUnitOfWork;

public class RepositoryCatalog implements iRepositoryCatalog{

	 private iUnitOfWork unitOfWork;
	 
	 public RepositoryCatalog(iUnitOfWork unitOfWork) {
	        this.unitOfWork = unitOfWork;
	    }

	@Override
	public iEnumerationValueRepository enumerations() {
		return new HsqlEnumerationValuesRepository(unitOfWork);
	}

	@Override
	public iUserRepository users() {
		return new UserRepository(unitOfWork);
	}

	@Override
	public iPersonRepository persons() {
		return new PersonRepository(unitOfWork);
	}

	@Override
	public iAddressRepository addresses() {
		return new AddressRepository(unitOfWork);
	}

	@Override
	public iNumberPhoneRepository numbers() {
		return new NumberPhoneRepository(unitOfWork);
	}

	@Override
	public iRolesRepository roles() {
		return new RolesRepository(unitOfWork);
	}

	@Override
	public void commit() {
		unitOfWork.saveChanges();
	}

	

}
