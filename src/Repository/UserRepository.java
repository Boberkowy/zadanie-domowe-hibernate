package Repository;


import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;


import Database.Entity;

import Database.User;
import Database.UserRoles;


import Repository.Interfaces.iUserRepository;
import UnitOfWork.iUnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;

public class UserRepository extends Repository<User> implements iUserRepository, iUnitOfWorkRepository {

	private iUnitOfWork unitofwork;
	private Session session; // session 
	private Query query;
	
	public UserRepository(iUnitOfWork unitOfWork) {
		super(unitOfWork);
		this.unitofwork = unitOfWork;
		this.session = unitOfWork.getSession();
	
	
	}


	@Override
	public void withId(int id) {
			String sql = "FROM User WHERE Id = :id ";
			query = session.createQuery(sql);
			query.setParameter("Id", id);
			System.out.println(query.list());
	}


	@Override
	public void allOnPage(PagingInfo page) {
        PagingInfo<User> pagingInfo = new PagingInfo<>(getAll());
		
	}


	@Override
	public void add(User entity) {
		unitofwork.markAsNew(entity, this);
		
	}


	@Override
	public void delete(User entity) {
		unitofwork.markAsDelete(entity, this);
	}


	@Override
	public void modify(User entity) {
		unitofwork.markAsDelete(entity, this);
		
	}


	@Override
	public List<User> getAll() {
		String sql = "FROM User";
		query = session.createQuery(sql);
		return(query.list());
	}


	@Override
	public int count() {
		return getAll().size();
	}


	@Override
	public void persistAdd(Entity entity) {
		session.persist(entity);
		
	}


	@Override
	public void persistDelete(Entity entity) {
		session.delete(entity);
		
	}


	@Override
	public void persistUpdate(Entity entity) {
		session.update(entity);
	}


	@Override
	public void withLogin(String login) {
		 String sql = "FROM User WHERE login = :login";
	     query=session.createQuery(sql);
	     query.setParameter("login",login);
	     System.out.println(query.list());
		
	}


	@Override
	public void withLoginAndPassword(String login, String password) {
		 String sql = "FROM User WHERE login = :login AND Password = :password";
	     query=session.createQuery(sql);
	     query.setParameter("login",login);
	     query.setParameter("Password", password);
	     System.out.println(query.list());
	}


	@Override
	public void setupPermissions(User user) {
		 String sql = "UPDATE  User set Roles = :roles WHERE User = :user";
	        query=session.createQuery(sql);
	        query.setParameter("Roles",new UserRoles());
	        query.setParameter("User", user);
	        System.out.println(query.list());
		
	}


	@Override
	protected void setUpUpdateQuery(User entity) throws SQLException {
		session.update(entity.getId());
		session.update(entity.getLogin());
		session.update(entity.getPassword());
		
	}


	@Override
	protected void setUpInsertQuery(User entity) throws SQLException {
		session.persist(entity.getId());
		session.persist(entity.getLogin());
		session.persist(entity.getPassword());
		
	}


	@Override
	protected String getTableName() {
		return "User";
	}


	@Override
	protected String getUpdateQuery() {
		 return "UPDATE User SET"
			 		+ "(userId,login,password)"
			 		+ "=(?,?,?)";
	}


	@Override
	protected String getInsertQuery() {
		return "INSERT INTO User VALUES"
		 		+ "(userId,login,password)"
		 		+ "=(?,?,?)";
	}


		
	}


	
