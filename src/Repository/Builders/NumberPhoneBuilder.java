package Repository.Builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.NumberPhone;
import Repository.Interfaces.iEntityBuilder;

public class NumberPhoneBuilder implements iEntityBuilder<NumberPhone>{

		
	@Override
	public NumberPhone build(ResultSet rs) throws SQLException {
		NumberPhone number = new NumberPhone();

		number.setId(rs.getInt("numberPhoneId"));
		number.setCountryPrefix(rs.getInt("countryPrefix"));
		number.setCityPrefix(rs.getInt("cityPrefix"));
		number.setNumber(rs.getString("Number"));
		number.setTypeId(rs.getInt("typeId"));
		return number;
	}
	

}
