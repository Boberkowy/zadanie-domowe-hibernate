package Repository.Builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.EnumerationValue;
import Repository.Interfaces.iEntityBuilder;

public class EnumerationValueBuilder implements iEntityBuilder<EnumerationValue> {

	
	@Override
	public EnumerationValue build(ResultSet rs) throws SQLException {
		EnumerationValue enumValue = new EnumerationValue();

		enumValue.setId(rs.getInt("id"));
		enumValue.setIntkey(rs.getInt("intKey"));
		enumValue.setStringKey(rs.getString("StringKey"));
		enumValue.setValue(rs.getString("Value"));
		enumValue.setEnumerationName(rs.getString("EnumerationValue"));
		return enumValue;
	}

}
