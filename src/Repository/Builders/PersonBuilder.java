package Repository.Builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.Person;
import Repository.Interfaces.iEntityBuilder;

public class PersonBuilder implements iEntityBuilder<Person>{

	
	@Override
	public Person build(ResultSet rs) throws SQLException {
		Person person = new Person();

		person.setFirstName(rs.getString("firstName"));
		person.setSurname(rs.getString("surname"));
		person.setPesel(rs.getString("Pesel"));
		person.setNip(rs.getString("Nip"));
		person.setEmail(rs.getString("Email"));
		person.setDateOfBirth(rs.getDate("DateOfBirth"));
		person.setId(rs.getInt("id"));

		return person;
	}

}
