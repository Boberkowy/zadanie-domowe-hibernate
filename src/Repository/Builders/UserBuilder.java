package Repository.Builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.User;
import Repository.Interfaces.iEntityBuilder;

public class UserBuilder implements iEntityBuilder<User>{


	
	@Override
	public User build(ResultSet rs) throws SQLException {
			User user = new User();
			user.setId(rs.getInt("id"));
			user.setLogin(rs.getString("login"));
			user.setPassword(rs.getString("password"));
			return user;
	}

}
