package Repository.Builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.RolesPermissions;

import Repository.Interfaces.iEntityBuilder;

public class RolePermissionBuilder implements iEntityBuilder<RolesPermissions>{

	@Override
	public RolesPermissions build(ResultSet rs) throws SQLException {
		RolesPermissions roles = new RolesPermissions();
		roles.setPermissionId(rs.getInt("permissionId"));
		roles.setRoleId(rs.getInt("roleId"));
		
		return roles;
	}

}
