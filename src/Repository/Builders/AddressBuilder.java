package Repository.Builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.Address;
import Database.Entity;
import Repository.Interfaces.iEntityBuilder;

public class AddressBuilder implements iEntityBuilder<Address> {
	

	@Override
	public Address build(ResultSet rs) throws SQLException {
		Address address = new Address();

		 address.setAddressId(rs.getInt("addressId"));
	     address.setCountryId(rs.getInt("countryId"));
	     address.setRegionId(rs.getInt("regionId"));
	     address.setCity(rs.getString("city"));
	     address.setHouseNumber(rs.getInt("houseNumber"));
	     address.setLocalNumer(rs.getInt("localNumber"));
	     address.setZipCode(rs.getString("zipcode"));
	     address.setTypeId(rs.getInt("typeId"));
		return address;
	}
    
}
