package Repository.Builders;

import java.sql.ResultSet;
import java.sql.SQLException;

import Database.User;
import Database.UserRoles;
import Repository.Interfaces.iEntityBuilder;

public class UserRolesBuilder implements iEntityBuilder<UserRoles> {

	@Override
	public UserRoles build(ResultSet rs) throws SQLException {
		UserRoles roles = new UserRoles();
		roles.setRoleId(rs.getInt("roleId"));
		
		return roles;
	}

}
