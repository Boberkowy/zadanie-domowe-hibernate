
package Repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Id;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import Database.Entity;
import Database.Person;
import Database.User;
import Repository.Interfaces.iPersonRepository;
import Repository.Interfaces.iRepository;
import UnitOfWork.iUnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;

public class PersonRepository extends Repository<Person> implements iPersonRepository, iUnitOfWorkRepository{
	
	private iUnitOfWork unitofwork;
	private Session session; // session favotry 
	private Query query;	
	
	protected PersonRepository(iUnitOfWork unitofwork) {
		super(unitofwork);
		this.unitofwork = unitofwork;
		this.session = unitofwork.getSession();
		
	}

	@Override
	public void allOnPage(PagingInfo page) {
        PagingInfo<Person> pagingInfo = new PagingInfo<>(getAll());
		
	}

	@Override
	public void add(Person entity) {
		unitofwork.markAsNew(entity,this);
	}

	@Override
	public void delete(Person entity) {
		unitofwork.markAsDelete(entity, this);
	}

	@Override
	public void modify(Person entity) {
		unitofwork.markAsChanged(entity, this);
		
	}

	@Override
	public List<Person> getAll() {
		String sql = "FROM Person ";
        query=session.createQuery(sql);
        return query.list();
	}

	@Override
	public int count() {
		return getAll().size();
	}

	@Override
	public void persistAdd(Entity entity) {
			session.persist(entity);
	}

	@Override
	public void persistDelete(Entity entity) {
			session.delete(entity);
		
	}

	@Override
	public void persistUpdate(Entity entity) {
			session.update(entity);
		
	}

	@Override
	public void withId(int id) {
		String sql = "FROM Person WHERE Id = :id";
		query = session.createQuery(sql);
		query.setParameter("Id", id);
		System.out.println(query.list());
	}

	@Override
	public void withNameAndSurname(String name, String surname) {
		String sql = "FROM Person WHERE Name = :name AND Surname = :surname ";
		query = session.createQuery(sql);
		query.setParameter("name", name);
		query.setParameter("Surname", surname);
		System.out.println(query.list());
	
	}

	@Override
	public void withPesel(long pesel) {
		String sql = "FROM Person WHERE Pesel = :pesel";
		query = session.createQuery(sql);
		query.setParameter("Pesel", pesel);
		System.out.println(query.list());
		
	}

	@Override
	public void withNip(long nip) {
		String sql = "FROM Person WHERE Nip = :nip";
		query = session.createQuery(sql);
		query.setParameter("Nip", nip);
		System.out.println(query.list());

		
	}

	@Override
	public void withEmail(String email) {
		String sql = "FROM Person WHERE Email = :email";
		query = session.createQuery(sql);
		query.setParameter("Email", email);
		System.out.println(query.list());

		
	}

	@Override
	public void withUser(User user) {
		String sql = "FROM Person WHERE User = :user";
		query = session.createQuery(sql);
		query.setParameter("User", user);
		System.out.println(query.list());

		
	}

	@Override
	protected void setUpUpdateQuery(Person entity) throws SQLException {
		session.update(entity.getPersonId());
		session.update(entity.getFirstName());
		session.update(entity.getSurname());
		session.update(entity.getPesel());
		session.update(entity.getEmail());
		session.update(entity.getDateOfBirth());
		session.update(entity.getNip());
		
	}

	@Override
	protected void setUpInsertQuery(Person entity) throws SQLException {
		session.persist(entity.getPersonId());
		session.persist(entity.getFirstName());
		session.persist(entity.getSurname());
		session.persist(entity.getPesel());
		session.persist(entity.getEmail());
		session.persist(entity.getDateOfBirth());
		session.persist(entity.getNip());
		
	}

	@Override
	protected String getTableName() {
		return "Person";
	}

	@Override
	protected String getUpdateQuery() {
		return "UPDATE Person SET "
		 		+ "(personId,firstName, surname, pesel,nip,email,dateOfBirth)"
		 		+ "=(?,?,?,?,?,?,?)";
	}

	@Override
	protected String getInsertQuery() {
		return "INSERT INTO Person VALUES"
		 		+ "(personId,firstName, surname, pesel,nip,email,dateOfBirth)"
		 		+ "=(?,?,?,?,?,?,?)";
	}

	

}
