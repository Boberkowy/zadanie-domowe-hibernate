package Repository;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import Database.Address;
import Database.Entity;
import Database.Person;
import Repository.Interfaces.iAddressRepository;
import Repository.Interfaces.iEntityBuilder;
import UnitOfWork.iUnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;

public class AddressRepository extends Repository<Address>  implements iAddressRepository {

	
	 private Session session;
	 private iUnitOfWork unitOfWork;
	 private Query query;
	 private iEntityBuilder<Address> builder;
	 
	 public AddressRepository(iUnitOfWork unitOfWork) {
	     super(unitOfWork);   
		 this.unitOfWork = unitOfWork;
	        this.session = unitOfWork.getSession();
	    }


	@Override
	public void allOnPage(PagingInfo page) {
		 PagingInfo<Address> pagingInfo = new PagingInfo<>(getAll());		
	}


	@Override
	public void add(Address entity) {
        unitOfWork.markAsNew(entity,this);
		
	}


	@Override
	public void delete(Address entity) {
        unitOfWork.markAsDelete(entity,this);
		
	}


	@Override
	public void modify(Address entity) {
        unitOfWork.markAsChanged(entity,this);
		
	}


	@Override
	public List<Address> getAll() {
			String sql = "From Address";
			query = session.createQuery(sql);
			return query.list();
			
	}


	public int count() {
        return getAll().size();
	}



	@Override
	public void withId(int id) {
		 String sql = "FROM Address WHERE addressId = :id";
	        query=session.createQuery(sql);
	        query.setParameter("id",id);
	        System.out.println(query.list());
		
	}


	@Override
	public void withCityAndStreet(String city, String street) {
		String sql = "FROM Address WHERE city = :city AND street = :street";
        query=session.createQuery(sql);
        query.setParameter("city",city);
        query.setParameter("street",street);
        System.out.println(query.list());
		
	}


	@Override
	public void withPerson(Person person) {
		String sql = "FROM Address WHERE person = :person";
        query=session.createQuery(sql);
        query.setParameter("person",person);
        System.out.println(query.list());
		
	}


	@Override
	protected void setUpUpdateQuery(Address entity) throws SQLException {
		session.update(entity.getAddressId());
	    session.update(entity.getCountryId());
	    session.update(entity.getRegionId());
	    session.update(entity.getCity());
	    session.update(entity.getStreet());
	    session.update(entity.getHouseNumber());
	    session.update(entity.getLocalNumer());
	    session.update(entity.getZipCode());
        session.update(entity.getTypeId());
		
	}


	@Override
	protected void setUpInsertQuery(Address entity) throws SQLException {
		session.persist(entity.getCountryId());
        session.persist(entity.getRegionId());
        session.persist(entity.getCity());
        session.persist(entity.getStreet());
        session.persist(entity.getHouseNumber());
        session.persist(entity.getLocalNumer());
        session.persist(entity.getZipCode());
        session.persist(entity.getTypeId());
	}


	@Override
	protected String getTableName() {
		return "Address";
	}


	@Override
	protected String getUpdateQuery() {
		 return "UPDATE Address SET"
		 		+ "(addressId,countryId, regionId, city, street houseNumber, localNumber, zipCode, typeId)"
		 		+ "=(?,?,?,?,?,?,?,?,?)";
	}


	

		@Override
		protected String getInsertQuery() {
			return "INSERT INTO Address "
			 		+ "(addressId,countryId, regionId, city, street houseNumber, localNumber, zipCode, typeId) VALUES"
			 		+ "=(?,?,?,?,?,?,?,?,?)";
		}
	}

