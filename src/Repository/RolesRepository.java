package Repository;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import Database.Address;
import Database.Entity;
import Database.RolesPermissions;
import Repository.Interfaces.iRolesRepository;
import UnitOfWork.iUnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;

public class RolesRepository extends Repository<RolesPermissions> implements iRolesRepository, iUnitOfWorkRepository {

	private Query query;
	private iUnitOfWork unitOfWork;
	private Session session;
	
	public RolesRepository(iUnitOfWork unitOfWork) {
		super(unitOfWork);
		this.unitOfWork = unitOfWork;
		this.session= unitOfWork.getSession();
	}

	
	
	@Override
	public void allOnPage(PagingInfo page) {
		 PagingInfo<RolesPermissions> pagingInfo = new PagingInfo<>(getAll());		
		
	}

	@Override
	public void add(RolesPermissions entity) {
		unitOfWork.markAsNew(entity,this);
		
	}

	@Override
	public void delete(RolesPermissions entity) {
		unitOfWork.markAsDelete(entity, this);
	}

	@Override
	public void modify(RolesPermissions entity) {
		unitOfWork.markAsChanged(entity, this);
		
	}

	@Override
	public List<RolesPermissions> getAll() {
		String sql = "FROM RolesPermissions";
		query = session.createQuery(sql);
		return query.list();
	}

	@Override
	public int count() {
		return getAll().size();
	}

	@Override
	public void persistAdd(Entity entity) {
		session.persist(entity);
		
	}

	@Override
	public void persistDelete(Entity entity) {
		session.delete(entity);
	}

	@Override
	public void persistUpdate(Entity entity) {
		session.update(entity);
		
	}

	@Override
	public void withId(int id) {
		String sql = "FROM RolesPermissions WHERE roleId = :id";
		query = session.createQuery(sql);
		query.setParameter("roleId", id);
		System.out.println(query.list());
		
	}

	@Override
	public void withPermissionId(int id) {
		String sql = "FROM RolesPermissions WHERE permissionId = : id";
		query = session.createQuery(sql);
		query.setParameter("permissionId", id);
		System.out.println(query.list());
	}



	@Override
	protected void setUpUpdateQuery(RolesPermissions entity) throws SQLException {
		session.update(entity.getPermissionId());
		session.update(entity.getRoleId());		
	}



	@Override
	protected void setUpInsertQuery(RolesPermissions entity) throws SQLException {
		session.persist(entity.getPermissionId());
		session.persist(entity.getRoleId());
		
	}



	@Override
	protected String getTableName() {
		return "RolesPermissions";
	}



	@Override
	protected String getUpdateQuery() {
		return "UPDATE RolesPermissions SET"
		 		+ "(roleId,PermissionId)"
		 		+ "=(?,?)";
	}



	@Override
	protected String getInsertQuery() {
		return "INSERT INTO RolesPermissions VALUES"
		 		+ "(roleId,PermissionId)"
		 		+ "=(?,?)";
	}

}
