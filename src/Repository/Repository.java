package Repository;




import java.sql.SQLException;
import java.util.List;

import org.hibernate.Session;

import Database.Entity;
import Database.EnumerationValue;
import Database.User;
import UnitOfWork.UnitOfWork;
import UnitOfWork.iUnitOfWork;
import UnitOfWork.iUnitOfWorkRepository;

import Repository.Interfaces.iEntityBuilder;
import Repository.Interfaces.iRepository;

public abstract class Repository<TEntity extends Entity>  implements iRepository<TEntity>, iUnitOfWorkRepository {

	private iUnitOfWork unitOfWork;
	private Session session;
	protected iEntityBuilder<TEntity> builder;
	
	
	protected Repository(iUnitOfWork unitOfWork){
		
		this.unitOfWork= unitOfWork;
		this.builder=builder;
		this.session = unitOfWork.getSession();
	}


	public void add(TEntity entity){
		unitOfWork.markAsNew(entity, this);

	}
	public void delete(TEntity entity){
		 
		unitOfWork.markAsDelete(entity, this);	
	}
		 
	public void modify (TEntity entity){
	
		unitOfWork.markAsChanged(entity,this);
	}

	

	public void persistAdd(Entity entity){
		session.persist(entity);
	}
	
	public void persistUpdate(Entity entity){
		session.update(entity);
		
	}
	
	public void persistDelete(Entity entity) {
		session.delete(entity);
	}
	
	public void allOnPage(PagingInfo page){
		
	}       
	
	public void withId(){
	}
	
	
	protected abstract void setUpUpdateQuery(TEntity entity) throws SQLException;
	protected abstract void setUpInsertQuery(TEntity entity) throws SQLException;
	protected abstract String getTableName();
	protected abstract String getUpdateQuery();
	protected abstract String getInsertQuery();
}
