package Repository;

import java.util.ArrayList;
import java.util.List;

import Database.Entity;

public class PagingInfo <TEntity extends Entity>{

	private int pageSize;
    private int page;
    private int totalCount;
    private List<TEntity> list;
    private int startIndex;
    private int endIndex;

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public PagingInfo(List<TEntity> list){
        this.list = new ArrayList<>();
        this.page = 1;
        this.totalCount = 1;

        calculatePages();
    }

    public void calculatePages() {
        if (pageSize > 0) {
            if (list.size() % pageSize == 0) {
                totalCount = list.size() / pageSize;
            } else {
                totalCount = (list.size() / pageSize) + 1;
            }
        }
    }

    
    public List<TEntity> getList() {
        return list;
    }

    
    public List<TEntity> getListOnPage() {
        return list.subList(startIndex,endIndex);
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
        calculatePages();
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        if (page >= totalCount) {
            this.page = totalCount;
        } else if (page <= 1) {
            this.page = 1;
        } else {
            this.page = page;
        }

        startIndex = pageSize * (page-1);
        if (startIndex < 0) {
            startIndex = 0;
        }
        endIndex = startIndex + pageSize;
        if (endIndex > list.size()) {
            endIndex = list.size();
        }
    }

    
    public int getPreviousPage() {
        if (page > 1) {
            return page-1;
        } else {
            return 0;
        }
    }

    
    public int getNextPage() {
        if (page < totalCount) {
            return page+1;
        } else {
            return 0;
        }
    }

    public int getTotalCount() {

        return totalCount;
    }

    public void setTotalCount(int totalCount){
        this.totalCount = totalCount;
    }
	
		
		
}
