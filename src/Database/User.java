package Database;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

@javax.persistence.Entity(name = "User")
public class User extends Entity implements Serializable{
 
	@Column(name="userid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int userId;
	private String login;
	private String password;

	@OneToMany
	private List<UserRoles> userRoles;
	
	@OneToOne
	private Person person;
	
	public User(){
		
	}
	
	public User(int userId, String login, String password, List<UserRoles> userRoles, Person person) {
		super();
		this.userId = userId;
		this.login = login;
		this.password = password;
		this.userRoles = userRoles;
		this.person = person;
	}
	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}	
	
	public void setLogin(String login) {
		this.login = login;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	public List<UserRoles> getUserRoles() {
		return userRoles;
	}
	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}

	
	
	
	
}
