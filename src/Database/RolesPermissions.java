package Database;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

@javax.persistence.Entity(name = "RolesPermissions")
public class RolesPermissions extends Entity implements Serializable{

	@Id
	private int permissionId;
	private int roleId;		
	@OneToMany
	
	private List<UserRoles> userRoles;

	public RolesPermissions(){
		
	}
	
	public RolesPermissions(int roleId, int permissionId, List<UserRoles> userRoles) {
		super();
		this.roleId = roleId;
		this.permissionId = permissionId;
		this.userRoles = userRoles;
	}
	public int getRoleId() {
		return roleId;
	}
	public int getPermissionId() {
		return permissionId;
	}
	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}
	public void setPermissionId(int permissionId) {
		this.permissionId = permissionId;
	}

	public List<UserRoles> getUserRoles() {
		return userRoles;
	}
	public void setUserRoles(List<UserRoles> userRoles) {
		this.userRoles = userRoles;
	}
	
	
}
