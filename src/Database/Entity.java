package Database;

import java.io.Serializable;

public abstract class Entity implements Serializable {

	private int id;
	private EntityState state;
	
	public int getId() {
		return id;
	}
	public EntityState getState() {
		return state;
	}
	public void setId(int id) {
		this.id = id;
	}
	public void setState(EntityState state) {
		this.state = state;
	}

	
}
