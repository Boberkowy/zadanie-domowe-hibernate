package Database;
 
import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "NumberPhone")
public class NumberPhone extends Entity  implements Serializable{
	@Column(name="userid", unique=true)
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int numberPhoneId;
	private int countryPrefix;
	private int cityPrefix;
	private String number;
	private int typeId;
	
	@OneToOne
	
	private Person person;
	
	
	public NumberPhone(){
		
	}
	
	
	public NumberPhone(int numberPhoneId, int countryPrefix, int cityPrefix, String number, int typeId, Person person) {
		super();
		this.numberPhoneId = numberPhoneId;
		this.countryPrefix = countryPrefix;
		this.cityPrefix = cityPrefix;
		this.number = number;
		this.typeId = typeId;
		this.person = person;
	}
	public int getNumberPhoneId() {
		return numberPhoneId;
	}
	public void setNumberPhoneId(int numberPhoneId) {
		this.numberPhoneId = numberPhoneId;
	}
	public int getCountryPrefix() {
		return countryPrefix;
	}
	public void setCountryPrefix(int countryPrefix) {
		this.countryPrefix = countryPrefix;
	}
	public int getCityPrefix() {
		return cityPrefix;
	}
	public void setCityPrefix(int cityPrefix) {
		this.cityPrefix = cityPrefix;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getTypeId() {
		return typeId;
	}
	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}

}
