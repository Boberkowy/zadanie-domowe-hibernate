package Database;

import java.io.Serializable;

import javax.persistence.*;

@javax.persistence.Entity(name = "Address")
public class Address extends Entity implements Serializable{
	
	@Column(name="userid", unique=true)
	@Id
	private int addressId;
	
	private int countryId;
	private int regionId;
	private String city;
	private String street;
	private int houseNumber;
	private int localNumer;
	private String zipCode;
	private int typeId;
	
	@OneToOne
	private Person person;
	
	
	
	public Address(){
		
	}
	
	public Address(int addressId, int countryId, int regionId, String city, String street, int houseNumber,
			int localNumer, String zipCode, int typeId, Person person) {
		super();
		this.addressId = addressId;
		this.countryId = countryId;
		this.regionId = regionId;
		this.city = city;
		this.street = street;
		this.houseNumber = houseNumber;
		this.localNumer = localNumer;
		this.zipCode = zipCode;
		this.typeId = typeId;
		this.person = person;
	}
	public int getAddressId() {
		return addressId;
	}

	public int getTypeId() {
		return typeId;
	}

	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}

	public void setTypeId(int typeId) {
		this.typeId = typeId;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getRegionId() {
		return regionId;
	}
	public void setRegionId(int regionId) {
		this.regionId = regionId;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public int getHouseNumber() {
		return houseNumber;
	}
	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}
	public int getLocalNumer() {
		return localNumer;
	}
	public void setLocalNumer(int localNumer) {
		this.localNumer = localNumer;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public int gettypeId() {
		return typeId;
	}
	public void setTyptypeId(int typtypeId) {
		typeId = typtypeId;
	}
	public Person getPerson() {
		return person;
	}
	public void setPerson(Person person) {
		this.person = person;
	}
	
}
