package Database;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

public class UserRoles extends Entity implements Serializable{
	

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int roleId;
	
	@OneToMany
	private List<RolesPermissions> rolePermissions;

	@OneToMany
	private List<User> users;
	
	public UserRoles(){
		
	}
	
	public UserRoles(int roleId, List<RolesPermissions> rolePermissions, List<User> users) {
		super();
		this.roleId = roleId;
		this.rolePermissions = rolePermissions;
		this.users = users;
	}
	public int getRoleId() {
		return roleId;

	}
	public void setRoleId(int userId) {
		this.roleId = userId;
	}
	public List<RolesPermissions> getRolePermissions() {
		return rolePermissions;
	}
	public void setRolePermissions(List<RolesPermissions> rolePermissions) {
		this.rolePermissions = rolePermissions;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}

	
	
}
