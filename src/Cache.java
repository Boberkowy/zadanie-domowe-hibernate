import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.hibernate.Session;

import Database.Entity;
import Database.EnumerationValue;
import Repository.RepositoryCatalog;
import Repository.Interfaces.iRepository;
import UnitOfWork.UnitOfWork;

public class Cache {
	 private Timer timer = new Timer();
	    RepositoryCatalog repositoryCatalog;
	    private Map<Entity,String> cachedObjects = new HashMap<>();
	    private static Cache cache;
	    private static Object token = new Object();

	    TimerTask timerTask = new TimerTask() {
	        @Override
	        public void run() {
	            resetCache();
	        }
	    };

	    private void setAutoRefreshingOfCache(){
	        timer.schedule(timerTask,5000L);
	    }

	    private Cache(){
	        setAutoRefreshingOfCache();
	    }

	    public static Cache getInstance(){

	        if(cache == null){
	            synchronized(token)
	            {
	                if(cache==null) {
	                    cache = new Cache();
	                }

	            }
	        }
	        return cache;
	    }

	    public static void resetCache(){
	        if(cache != null){
	            synchronized (token){
	                if (cache!=null){
	                    cache = new Cache();
	                }
	            }
	        }else {
	            getInstance();
	        }
	    }

	    public void clearCache(){
	        cachedObjects.clear();
	    }
	    public RepositoryCatalog getRepositoryCatalog() {
	        return repositoryCatalog;
	    }

	    public void setRepositoryCatalog(RepositoryCatalog repositoryCatalog) {
	        this.repositoryCatalog = repositoryCatalog;
	    }

	    public void getEnumerationNames(){
	       iRepository<EnumerationValue> repository = repositoryCatalog.enumerations();
	       List<EnumerationValue> names = repository.getAll();
	        for (EnumerationValue s : names) {
	            cachedObjects.put(s,s.getEnumerationName());
	            System.out.println(cachedObjects);
	        }
	    }


	    
}
